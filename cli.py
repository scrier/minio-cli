import click
from context import Context
from object.commands import object
from bucket.commands import bucket
from tags.commands import tags
from iam.commands import iam


context = Context()


@click.group('cli')
def cli():
    pass


cli.add_command(bucket)
cli.add_command(object)
cli.add_command(tags)
cli.add_command(iam)


def run_cli():
    cli()


if __name__ == "__main__":
    run_cli()
