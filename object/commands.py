import click
import os
from context import Context


context = Context()


@click.group('object')
def object():
    pass


@object.command('list')
@click.option('-b', '--bucket', 'bucket', required=True, type=str)
@click.option('-p', '--prefix', 'prefix', required=False, default=None, type=str)
@click.option('-r', '--recursive', 'recursive', is_flag=True, default=False, required=False, type=bool)
def object_list(bucket, prefix, recursive):
    response = context.client().list_objects(bucket_name=bucket,
                                   prefix=prefix,
                                   recursive=recursive)
    [click.echo(f._object_name) for f in response]


@object.command('upload')
@click.argument('filename')
@click.option('-b', '--bucket', 'bucket', required=True, type=str)
@click.option('-n', '--name', 'name', required=False, default=None, type=str)
@click.option('-l', '--length', 'length', default=None, required=False, type=int)
def object_upload(filename, bucket, name, length):
    if not os.path.isfile(filename):
        click.echo(f"Cannot find the filename {filename}")
    else:
        with open(filename, "rb") as input_stream:
            result = context.client().put_object(bucket_name=bucket,
                                       object_name=name or os.path.basename(input_stream.name),
                                       data=input_stream,
                                       length=length or os.stat(filename).st_size)
        click.echo(result.__dict__)
        click.echo(f'length is {os.stat(filename).st_size}')


@object.command('download')
@click.argument('filename')
@click.option('-b', '--bucket', 'bucket', required=True, type=str)
@click.option('-n', '--name', 'name', required=True, type=str)
def object_download(filename, bucket, name):
    with open(name or os.path.basename(filename), "wb") as output_stream:
        response = context.client().get_object(bucket_name=bucket,
                                     object_name=filename)
        for data in response:
            output_stream.write(data)
    output_stream.close()
