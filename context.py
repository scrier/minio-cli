from typing import Dict, Any
from minio import Minio
from kubernetes import client, config
import base64

import yaml


class Context(object):

    cfg: Dict[str, Any] = None
    host: str = None
    minio: Minio = None

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(Context, cls).__new__(cls)
        return cls.instance

    def __init__(self):
        with open("config.yaml", "r") as stream:
            self.cfg = yaml.safe_load(stream)

        self.host = self.cfg['host']

        config.load_kube_config()
        v1 = client.CoreV1Api()
        data = v1.read_namespaced_secret(self.cfg['secret'], self.cfg['namespace']).data
        # print(data)
        access = base64.b64decode(data["access-key"]).decode("UTF-8")
        secret = base64.b64decode(data["secret-key"]).decode("UTF-8")

        self.minio = Minio(
            self.host,
            access_key=access,
            secret_key=secret,
        )

    def client(self) -> Minio:
        return self.minio
