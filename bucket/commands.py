import click
from context import Context


context = Context()


@click.group('bucket')
def bucket():
    pass


@bucket.command('list')
def bucket_list():
    [click.echo(f.name) for f in context.client().list_buckets()]


@bucket.command('create')
@click.option('-n', '--name', 'name', required=True, type=str)
def bucket_create(name):
    click.echo(context.client().make_bucket(bucket_name=name))