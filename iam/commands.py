import click

from context import Context

context = Context()


@click.group('iam')
def iam():
    pass


@iam.command('get-bucket-policy')
@click.option('-b', '--bucket', 'bucket', required=True, type=str)
def get_bucket_policy(bucket):
    return context.client().get_bucket_policy(bucket)

