import click
from context import Context


context = Context()


@click.group('tags')
def tags():
    pass


@tags.command('list')
@click.argument('filename')
@click.option('-b', '--bucket', 'bucket', required=True, type=str)
def object_tags(filename, bucket):
    response = context.client().get_object_tags(bucket_name=bucket,
                                      object_name=filename)
    for k, v in response.items():
        click.echo(f"{k} => {v}")


@tags.command('create')
@click.argument('filename')
@click.option('-b', '--bucket', 'bucket', required=True, type=str)
@click.option('-t', '--tag', 'tags', multiple=True, required=True, type=str)
def object_download(filename, bucket, tags):
    click.echo('\n'.join(tags))
    input_tags = context.client().get_object_tags(bucket_name=bucket,
                                      object_name=filename)
    for tag in tags:
        if '=' in tag:
            splt = tag.split('=')
            click.echo(f"{splt[0]} = {splt[1]}")
            input_tags[splt[0]] = splt[1]
        elif ':' in tag:
            splt = tag.split(':')
            click.echo(f"{splt[0]} = {splt[1]}")
            input_tags[splt[0]] = splt[1]
        else:
            click.echo(f"Unknown tag: {tag}!!!")
    click.echo(input_tags)
    context.client().set_object_tags(bucket_name=bucket,
                           object_name=filename,
                           tags=input_tags)