#!/bin/bash

# Usage: ./minio-upload.sh my-bucket my-file.zip

bucket=$1
file=$2

host=https://s3-oslo.ostore.vegvesen.no:443
s3_key=$(kubectl get secret stm-iams3skriv-meridian --namespace default -o jsonpath='{.data.access-key}' | base64 -d)
s3_secret=$(kubectl get secret stm-iams3skriv-meridian --namespace default -o jsonpath='{.data.secret-key}' | base64 -d)

resource="/${bucket}/${file}"
content_type="application/octet-stream"
date=`date -R`
_signature="PUT\n\n${content_type}\n${date}\n${resource}"
signature=`echo -en ${_signature} | openssl sha1 -hmac ${s3_secret} -binary | base64`

curl -X PUT -T "${file}" \
          -H "Host: ${host}" \
          -H "Date: ${date}" \
          -H "Content-Type: ${content_type}" \
          -H "Authorization: AWS ${s3_key}:${signature}" \
          https://${host}${resource}
